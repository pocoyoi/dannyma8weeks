# DANNY MA's 8 WEEKS SQL CHALLENGE #

https://8weeksqlchallenge.com

## How to run ##
Dockerized instance of Danny Ma's 8 Weeks of SQL postgresDB.

## TOC ##
1. [Week 1](weeks/W1.md) 
2. [Week 2](weeks/W2.md)